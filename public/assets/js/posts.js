$.ajax({
    type: 'get',
    url: '/posts',
    success: function(response) {
        var html = template('postsTpl', { data: response });
        $('#postsBox').html(html);
        var page = template('pageTpl', response);
        $('#page').html(page);
    }
});



function changePage(page) {
    $.ajax({
        type: 'get',
        url: '/posts',
        data: {
            page: page
        },
        success: function(response) {
            var html = template('postsTpl', { data: response });
            $('#postsBox').html(html);
            var page = template('pageTpl', response);
            $('#page').html(page);
        }
    })
};

$.ajax({
    type: 'get',
    url: '/categories',
    success: function(response) {
        // console.log(response);
        var html = template('categoryTpl', { data: response });
        // console.log(html);
        $('#categoryBox').html(html);
    }
});

$('#filterForm').on('submit', function() {
    // alert(1);
    var formDate = $(this).serialize();
    $.ajax({
        type: 'get',
        url: '/posts',
        data: formDate,
        success: function(response) {
            var html = template('postsTpl', { data: response });
            $('#postsBox').html(html);
            var page = template('pageTpl', response);
            $('#page').html(page);
        }
    })

    return false;
})

$('#postsBox').on('click', '.delete', function() {
    var isConfirm = confirm('Do you want to delete the article?');
    if (isConfirm) {
        var id = $(this).attr('data-id');
        // console.log(id);
        $.ajax({
            type: 'delete',
            url: '/posts/' + id,
            success: function() {
                location.reload();
            }
        })
    }
})