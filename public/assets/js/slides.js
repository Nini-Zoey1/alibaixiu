$('#file').on('change', function() {
    var file = this.files[0];
    var formData = new FormData();
    formData.append('image', file);
    $.ajax({
        type: 'post',
        url: '/upload',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            // console.log(response[0].image);
            $('#image').val(response[0].image);
        }
    })
});

$('#slidesForm').on('submit', function() {
    var formData = $(this).serialize();
    $.ajax({
        type: 'post',
        url: '/slides',
        data: formData,
        success: function() {
            location.reload();
        }
    })
    return false;
})

$.ajax({
    type: 'get',
    url: '/slides',
    success: function(response) {
        // console.log(response);
        var html = template('slidesTpl', { data: response });
        // console.log(html);
        $('#slidesBox').html(html);

    }
});

$('#slidesBox').on('click', '.delete', function() {
    var isConfirm = confirm('Do you want to delete the slide?');
    if (isConfirm) {
        var id = $(this).attr('data-id');
        // console.log(id);
        $.ajax({
            type: 'delete',
            url: '/slides/' + id,
            success: function() {
                location.reload();
            }
        })

    }
})