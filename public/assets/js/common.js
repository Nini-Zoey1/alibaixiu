$('#logout').on('click', function() {
    var isConfirm = confirm('Do you want to logout?');
    if (isConfirm) {
        $.ajax({
            type: 'post',
            url: '/logout',
            success: function(response) {
                // console.log(123456);
                location.href = 'login.html';
            },
            error: function() {
                alert('Failed to logout!');
            }
        })
    }
});

function formatDate(date) {
    date = new Date(date);
    return (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate());
};

$.ajax({
    type: 'get',
    url: '/users/' + userId,
    success: function(response) {
        // console.log(response);
        $('.avatar').attr('src', response.avatar);
        $('.profile .name').html(response.nickName);
    }
})