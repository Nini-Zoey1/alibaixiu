$.ajax({
    type: 'get',
    url: '/posts/count',
    success: function(response) {
        // console.log(response);
        $('#post').html('<strong>' + response.postCount + '</strong> Articles（<strong>' + response.draftCount + '</strong> Drafts）')
    }
});

$.ajax({
    type: 'get',
    url: '/categories/count',
    success: function(response) {
        $('#category').html('<strong>' + response.categoryCount + '</strong> Categories')
    }
});

$.ajax({
    type: 'get',
    url: '/comments/count',
    success: function(response) {
        $('#comment').html('<strong>' + response.commentCount + '</strong> Comments')
    }
});