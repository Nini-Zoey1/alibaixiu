$.ajax({
    type: 'get',
    url: '/comments',
    success: function(response) {
        // console.log(response);
        var html = template('commentsTpl', response)
            // console.log(html);
        $('#commentsBox').html(html);
        var pageHtml = template('pageTpl', response);
        console.log(pageHtml);
        $('#pageBox').html(pageHtml);
    }
});

function changePage(page) {
    $.ajax({
        type: 'get',
        url: '/comments',
        data: {
            page: page
        },
        success: function(response) {
            // console.log(response);
            var html = template('commentsTpl', response)
                // console.log(html);
            $('#commentsBox').html(html);
            var pageHtml = template('pageTpl', response);
            // console.log(pageHtml);
            $('#pageBox').html(pageHtml);
        }
    });
};

$('#commentsBox').on('click', '.status', function() {
    var status = $(this).attr('data-status');
    var id = $(this).attr('data-id');
    // alert(status);
    $.ajax({
        type: 'put',
        url: '/comments/' + id,
        data: {
            state: status == 0 ? 1 : 0
        },
        success: function() {
            location.reload();
        }
    })
});

$('#commentsBox').on('click', '.delete', function() {
    var isConfirm = confirm('Do you want to remove the comment?');
    if (isConfirm) {
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'delete',
            url: '/comments/' + id,
            success: function() {
                location.reload();
            }
        })
    }
});