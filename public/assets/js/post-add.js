$.ajax({
    type: 'get',
    url: '/categories',
    success: function(response) {
        // console.log(response);
        var html = template('categoryTpl', { data: response });
        // console.log(html);
        $('#category').html(html);
    }
});

$('#feature').on('change', function() {
    var file = this.files[0];
    var formData = new FormData();
    formData.append('cover', file);
    $.ajax({
        type: 'post',
        url: '/upload',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            $('#thumbnail').val(response[0].cover);
        }
    })
});

$('#addForm').on('submit', function() {
    var formData = $(this).serialize();
    // console.log(formData);

    $.ajax({
        type: 'post',
        url: '/posts',
        data: formData,
        success: function(response) {
            location.href = '/admin/posts.html';
        }
    })
    return false;
});

var id = getUrlParams('id');
if (id != -1) {
    // console.log(id);
    $.ajax({
        type: 'get',
        url: '/posts/' + id,
        success: function(response) {
            $.ajax({
                type: 'get',
                url: '/categories',
                success: function(categories) {
                    response.categories = categories;
                    // console.log(response);
                    var html = template('modifyTpl', response);
                    // console.log(html);
                    $('#parentBox').html(html);
                }
            });
        }
    });
};

$('#parentBox').on('submit', '#modifyForm', function() {
    var formData = $(this).serialize();
    console.log(formData);

    id = $(this).attr('data-id');
    console.log(id);
    $.ajax({
        type: 'put',
        url: '/posts/' + id,
        data: formData,
        success: function() {
            location.href = '/admin/posts.html';
        }
    })
    return false;
})

function getUrlParams(name) {
    var paramsAry = location.search.substr(1).split('&');
    for (var i = 0; i < paramsAry.length; i++) {
        if (paramsAry[i].split('=')[0] == name) {
            return paramsAry[i].split('=')[1];
        }
    }
    return -1;
}