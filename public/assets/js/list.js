function getUrlParams(name) {
    var paramsAry = location.search.substr(1).split('&');
    for (var i = 0; i < paramsAry.length; i++) {
        if (paramsAry[i].split('=')[0] == name) {
            return paramsAry[i].split('=')[1];
        }
    }
    return -1;
};

var categoryId = getUrlParams('categoryId');

$.ajax({
    type: 'get',
    url: '/posts/category/' + categoryId,
    success: function(response) {
        // console.log(response);
        var html = template('listTpl', { data: response });
        $('#listBox').html(html);

    }
})