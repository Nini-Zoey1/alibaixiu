$('#addCategory').on('submit', function() {
    var formData = $(this).serialize();
    console.log(formData);

    $.ajax({
        type: 'post',
        url: '/categories',
        data: formData,
        success: function() {
            location.reload();
        }
    })

    return false;
});

$.ajax({
    type: 'get',
    url: '/categories',
    success: function(response) {
        // console.log(response);
        var html = template('categoryListTpl', { data: response });
        // console.log(html);
        $('#categoryBox').html(html);
    }
});

$('#categoryBox').on('click', '.edit', function() {
    var id = $(this).attr('data-id');
    $.ajax({
        type: 'get',
        url: '/categories/' + id,
        success: function(response) {
            var html = template('modifyCategoryTpl', response);
            $('#formBox').html(html);
        }
    })
});

$('#formBox').on('submit', '#modifyCategory', function() {
    var formData = $(this).serialize();
    console.log(formData);

    var id = $(this).attr('data-id');
    $.ajax({
        type: 'put',
        url: '/categories/' + id,
        data: formData,
        success: function() {
            location.reload();
        }
    })
    return false;
});

$('#categoryBox').on('click', '.delete', function() {
    var isConfirm = confirm("Do you want to delete the category?");
    var id = $(this).attr('data-id');
    if (isConfirm) {
        $.ajax({
            type: 'delete',
            url: '/categories/' + id,
            success: function() {
                location.reload();
            }
        })
    }
})